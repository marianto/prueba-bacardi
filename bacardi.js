let txt = document.getElementById('lima_txt')
let target = document.getElementById('limas');
let result = document.getElementById('jugo');
let vaso = document.getElementById('vaso');
let hojas_menta = document.getElementById('menta');
let menta_txt = document.getElementById('menta_txt')

target.addEventListener('animationend', AnimationLima, false);
vaso.addEventListener('animationend', AnimationLima, false);
result.addEventListener('animationend', AnimationLima, false);
txt.addEventListener('animationend', AnimationLima, false);

function startAnimation() {
    target.style.animation = "showLima 2s";
    hojas_menta.style.opacity = 1;
    menta_txt.style.opacity = 1;


}


function AnimationLima() {
    console.log('Done');
    vaso.style.visibility = "hidden";
    target.style.visibility = "hidden";
    txt.style.visibility = "hidden";
    result.style.opacity = 1;
}


let result_menta = document.getElementById('hojas_menta');
hojas_menta.addEventListener('animationend', animationMenta, false);
hojas_menta.addEventListener('animationstart', showMenta, false);
result_menta.addEventListener('animationend', animationMenta, false);
let bacardi = document.getElementById('bacardi');
let bacardi_txt = document.getElementById('bacardi_txt')


function showMenta() {
    console.log('mentaa');
    bacardi.style.opacity = 1;

}

function startAnimationMenta() {
    hojas_menta.style.animation = "showMenta 2s";



}
function animationMenta() {
    console.log('Animatioooooooooon');
    hojas_menta.style.visibility = "hidden";
    result_menta.style.opacity = 1;
    menta_txt.style.visibility = "hidden";
    bacardi_txt.style.opacity = 1;


}

let result_bacardi = document.getElementById('botella_bacardi');
let hielo = document.getElementById('hielo');
bacardi.addEventListener('animationend', animationBacardi, false);
result_bacardi.addEventListener('animationend', animationBacardi, false);
let hielo_txt = document.getElementById('hielo_txt')

function startAnimationBacardi() {
    bacardi.style.animation = "showBacardi 2s linear";
    hielo.style.opacity = 1;

}

function animationBacardi() {
    console.log('Bacardiiiiiiiii');
    bacardi.style.visibility = "hidden";
    result_bacardi.style.opacity = 1;
    bacardi_txt.style.visibility = "hidden";
    hielo_txt.style.opacity = 1;


}

let result_hielo = document.getElementById('vaso-hielo');
let soda = document.getElementById('soda');
let soda_txt = document.getElementById('soda_txt');
result_hielo.addEventListener('animationend', animationHielo, false);
hielo.addEventListener('animationend', animationHielo, false);

function startAnimationIce() {
    hielo.style.animation = "showHielo 2s";
    soda.style.opacity = 0.8;
}

function animationHielo() {
    hielo.style.visibility = "hidden";
    result_hielo.style.opacity = 1;
    hielo_txt.style.visibility = "hidden";
    soda_txt.style.opacity = 1;



}

let result_soda = document.getElementById('vaso-soda');
let drag_event = document.getElementById('drag_event');
let drag_arrow = document.getElementById('drag_arrow');
let primavera = document.getElementById('primavera');
let txt3 = document.getElementById('enhorabuena');
let button = document.getElementById('button');
result_soda.addEventListener('animationend', animationSoda, false);
soda.addEventListener('animationend', animationSoda, false);


function startAnimationSoda() {
    soda.style.animation = "showSoda 2s";
}



function animationSoda() {
    soda.style.visibility = "hidden";
    result_soda.style.opacity = 1;
    soda_txt.style.visibility = "hidden";
    drag_event.style.visibility = "hidden";
    drag_arrow.style.visibility = "hidden";
    primavera.style.visibility = "hidden";
    txt3.style.opacity = 1;
    button.style.opacity = 1;
}




















